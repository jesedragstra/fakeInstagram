package com.jesse.fakeinstagram.exception;

public class LikesException extends RuntimeException {
    public LikesException(String message){
        super(message);
    }
}
