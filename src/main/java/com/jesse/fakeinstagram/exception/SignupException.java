package com.jesse.fakeinstagram.exception;

public class SignupException extends RuntimeException {
    public SignupException(String message){
        super(message);
    }
}
