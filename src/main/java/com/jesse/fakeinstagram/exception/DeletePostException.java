package com.jesse.fakeinstagram.exception;

public class DeletePostException extends RuntimeException{
    public DeletePostException(){
        super("Unable to delete post");
    }
}
