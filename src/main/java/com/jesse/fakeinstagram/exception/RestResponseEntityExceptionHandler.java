package com.jesse.fakeinstagram.exception;


import com.jesse.fakeinstagram.model.ErrorResponse;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {
    // 404
    @ExceptionHandler({ NoHandlerFoundException.class })
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity notFound(final NoHandlerFoundException ex) {
        ErrorResponse err = ErrorResponse.builder().statusCode("P-404").message(ex.getMessage())
                .metadata("{additionalInfo:'Unable to find resource'}").build();
        return new ResponseEntity<>(err,HttpStatus.NOT_FOUND );
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ExceptionHandler(value = LoginException.class)
    public ResponseEntity<ErrorResponse> loginExceptionHanlder(LoginException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-404").message(ex.getMessage())
                .metadata("{additionalInfo:'Unnable to provide more information'}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(value = LikesException.class)
    public ResponseEntity<ErrorResponse> likesExceptionHandler(LikesException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-614").message(ex.getMessage())
                .metadata("{additionalInfo:'Unnable to provide more information'}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(value = DeletePostException.class)
    public ResponseEntity<ErrorResponse> deleteExceptionHandler(DeletePostException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-604").message(ex.getMessage())
                .metadata("{additionalInfo:'Unnable to provide more information'}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(value = SavePostException.class)
    public ResponseEntity<ErrorResponse> savePostExceptionHandler(SavePostException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-504").message(ex.getMessage())
                .metadata("{additionalInfo:'Unnable to provide more information'}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(value = DeleteException.class)
    public ResponseEntity<ErrorResponse> deleteExceptionHandler(DeleteException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-504").message(ex.getMessage())
                .metadata("{additionalInfo:'Unnable to provide more information'}").build();
        return new ResponseEntity<>(err,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<ErrorResponse> usernameNotFound(UsernameNotFoundException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-501").message("Unable to login")
                .metadata("{additionalInfo"+ex.getMessage()+"}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(value = SignupException.class)
    public ResponseEntity<ErrorResponse> emailExists(SignupException ex){
        ErrorResponse err = ErrorResponse.builder().statusCode("P-601").message("Unable to create Account")
                .metadata("{additionalInfo"+ex.getMessage()+"}").build();
        return new ResponseEntity<>(err,HttpStatus.UNAUTHORIZED);
    }
}
