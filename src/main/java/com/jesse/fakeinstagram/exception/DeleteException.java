package com.jesse.fakeinstagram.exception;

public class DeleteException extends RuntimeException{
    public DeleteException(String message){
        super(message);
    }
}
