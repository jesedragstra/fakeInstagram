package com.jesse.fakeinstagram.exception;

public class SavePostException extends RuntimeException{
    public SavePostException(String message){
        super(message);
    }
}
