package com.jesse.fakeinstagram.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Post {
    @Id
    @GeneratedValue
    @Column(name="POST_ID")
    private UUID id;
    @Column(name = "POST_LINK")
    private String link;
    @Temporal(TemporalType.DATE)
    private Calendar date;
    @ElementCollection
    @Column(name = "COMMENTS_LIST")
    private List<String> comments;
    @ElementCollection
    @Column(name = "POST_LIKES")
    private List<String> likes;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "instagramuser")
    private InstagramUsers user;
    public Post retrievePosts(){
        user=null;
        return this;
    }
}
