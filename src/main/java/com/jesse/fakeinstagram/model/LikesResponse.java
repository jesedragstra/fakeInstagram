package com.jesse.fakeinstagram.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LikesResponse {
    private int likeCount;
    private List<String> usersLikes;
}
