package com.jesse.fakeinstagram.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {
    private String statusCode;
    private String metadata;
    private String message;
}
