package com.jesse.fakeinstagram.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InstagramUsers {
    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private UUID id;
    @Column(name = "ACCOUNT_PASSWORD",nullable = false)
    @NotNull
    private String password;
    @Column(name="ACCOUNT_USERNAME",nullable = false,unique = true)
    @NotNull
    private String username;
    @Column(name="ACCOUNT_NAME",nullable = false)
    @NotNull
    private String user;
    @Column(name="ACCOUNT_SECOND_NAME",nullable = false)
    @NotNull
    private String secondName;
    @Column(name="ACCOUNT_THIRD_NAME")
    private String thirdName;
    @OneToMany(mappedBy = "user",
            orphanRemoval = true,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Post> posts =  new ArrayList<>();;
    @Column(name="ACCOUNT_EMAIL",unique = true)
    @NotNull(message = "{email.required}" )
    @Pattern(regexp = "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9]"
            + "(?:[A-Za-z0-9-]*[A-Za-z0-9])?",
            message = "{invalid.email}")
    @NotNull
    private String email;

}
