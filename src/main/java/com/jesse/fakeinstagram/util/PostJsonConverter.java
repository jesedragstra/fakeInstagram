package com.jesse.fakeinstagram.util;

import com.jesse.fakeinstagram.model.Post;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Access;
import javax.persistence.AttributeConverter;
import java.util.List;
import java.util.UUID;

@Component
public class PostJsonConverter implements AttributeConverter<JSONObject, List<Post>>{
    @Override
    public List<Post> convertToDatabaseColumn(JSONObject jsonObject) {
        return null;
    }

    @Override
    public JSONObject convertToEntityAttribute(List<Post> postList) {
        JSONObject postObject = new JSONObject();
        try{
            for(int i =0;i<postList.size();i++){
                Post currentPost = postList.get(i);
                UUID postId = currentPost.getId();
                String postLink = currentPost.getLink();
                postObject.put("postId",postId);
                postObject.put("postLink",postLink);
            }
            JSONObject currentObject = postObject;
            postObject = new JSONObject();
            return currentObject;

        }catch (Exception e){
            throw new Error("Unable to create Object");
        }
    }
}
