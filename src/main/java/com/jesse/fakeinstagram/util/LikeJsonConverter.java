package com.jesse.fakeinstagram.util;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.AttributeConverter;

@Component
public class LikeJsonConverter implements AttributeConverter<JSONObject,String> {
    @Override
    public String convertToDatabaseColumn(JSONObject jsonData){
        String json;
        try {
            String postId = jsonData.getString("postId");
            String userId = jsonData.getString("userId");
            json = jsonData.toString();
        } catch (JSONException e) {
            json = "";
        }
        return json;
    }
    @Override
    public JSONObject convertToEntityAttribute(String jsonDataAsJson) {
        JSONObject jsonData;
        try {
            jsonData = new JSONObject(jsonDataAsJson);
        } catch (JSONException ex) {
            jsonData = null;
        }
        return jsonData;
    }
}
