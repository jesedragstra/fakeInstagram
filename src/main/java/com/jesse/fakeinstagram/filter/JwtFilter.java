package com.jesse.fakeinstagram.filter;

import com.jesse.fakeinstagram.service.UserService;
import com.jesse.fakeinstagram.util.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {
    private JWTUtility jwtUtility;
    private UserService userService;
    @Autowired
    public void setJwtUtility(JWTUtility jwtUtility) {
        this.jwtUtility = jwtUtility;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, UsernameNotFoundException,IOException {
         String authorization = request.getHeader("Authorization");
         String token = null;
         String username = null;
         if(null != authorization && authorization.startsWith("Bearer")){
             token = authorization.substring(7);
             username = jwtUtility.getUsernameFromToken(token);
         }
         if(null != username && SecurityContextHolder.getContext().getAuthentication() == null){
             UserDetails userDetails = userService.loadByUsername(username);
             if(jwtUtility.validateToken(token,userDetails)){
                 UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                 usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                 SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
             }
             response.setHeader("username",userDetails.getUsername());
         }
         filterChain.doFilter(request,response);
    }
}
