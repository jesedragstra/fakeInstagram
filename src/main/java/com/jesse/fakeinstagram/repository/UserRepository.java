package com.jesse.fakeinstagram.repository;

import com.jesse.fakeinstagram.model.Post;
import com.jesse.fakeinstagram.model.InstagramUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<InstagramUsers, UUID> {
    @Query(value = "SELECT u.posts FROM USERS u INNER JOIN POSTS p p.USER_ID=?1",nativeQuery = true)
    public List<Post> retrieveAllPosts(Long userId);
    @Transactional
    public InstagramUsers findByUsername(String username);
    public InstagramUsers findByEmail(String email);
    @Transactional
    public void deleteByUsername(String username);
}
