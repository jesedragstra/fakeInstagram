package com.jesse.fakeinstagram.repository;

import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PostRepository extends JpaRepository<Post, UUID> {
    public List<Post> findByUser(InstagramUsers user);
    public Post findByUserAndId(InstagramUsers user,UUID postId);
    @Transactional
    public void deleteByIdAndUser(UUID uuid,InstagramUsers post);
    public Optional<Post> findById(UUID postId);
}
