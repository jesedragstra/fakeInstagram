package com.jesse.fakeinstagram.controller;

import com.jesse.fakeinstagram.model.CreatePostRequest;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.model.Post;
import com.jesse.fakeinstagram.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

@RestController()
public class Posts {
    PostService postService;
    @Autowired
    public void setPostService(PostService postService){
        this.postService = postService;
    }
    @PostMapping("/create")
   public Post createPost(@RequestBody CreatePostRequest createPostRequest, HttpServletResponse response){
       Post post = postService.postPicture(createPostRequest.getPostLink(),response.getHeader("username"));
       return post.retrievePosts();
   }
   @GetMapping("/retrievePosts")
    public List<Post> retrievePosts(@RequestParam String username){
        List<Post> post = postService.retrieveImages(username);;
        for(int i=0;i<post.size();i++){
            Post onePost = post.get(i);
            onePost.retrievePosts();
            post.set(i,onePost);
        }
        return post;
   }
   @DeleteMapping("/deletePost")
    public Post deletePost(@RequestParam UUID postId, HttpServletResponse response){
        Post post = postService.deletePost(postId,response.getHeader("username"));
        return post;
   }

}
