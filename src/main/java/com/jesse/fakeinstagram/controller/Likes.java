package com.jesse.fakeinstagram.controller;

import com.jesse.fakeinstagram.model.LikesResponse;
import com.jesse.fakeinstagram.model.Post;
import com.jesse.fakeinstagram.service.LikeService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
public class Likes {
    LikeService likeService;
    @Autowired
    public void setLikeService(LikeService likeService){
        this.likeService = likeService;
    }
    @PostMapping("/like")
    public Boolean likePost(@RequestParam UUID postId, HttpServletResponse response){
        likeService.LikePost(response.getHeader("username"),postId);
        return true;
    }
    @DeleteMapping("/deleteLike")
    public Boolean deleteLike(@RequestParam UUID postId, HttpServletResponse response){
        likeService.deleteLike(response.getHeader("username"),postId);
        return true;
    }
    @GetMapping("/retrieveLikes")
    public LikesResponse retrieveLikes(@RequestParam UUID postId){
        return likeService.retrieveAllLikes(postId);
    }
}