package com.jesse.fakeinstagram.controller;

import com.jesse.fakeinstagram.exception.LoginException;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.model.JwtRequest;
import com.jesse.fakeinstagram.model.JwtResponse;
import com.jesse.fakeinstagram.service.UserService;
import com.jesse.fakeinstagram.util.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class HomeController {
    private JWTUtility jwtUtility;
    private AuthenticationManager authenticationManager;
    private UserService userService;
    @Autowired
    public void HomeController(JWTUtility jwtUtility,AuthenticationManager authenticationManager,UserService userService){
        this.jwtUtility = jwtUtility;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }
    @PostMapping("/login")
    public JwtResponse authenticated(@RequestBody JwtRequest jwtRequest) throws Exception {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(),jwtRequest.getPassword()));

        }catch(Exception e){
            throw new LoginException("Unable to login");
        }
        final UserDetails userDetails = userService.loadByUsername(jwtRequest.getUsername());
        final String token = jwtUtility.generateToken(userDetails);
        return new JwtResponse(token);
    }
}
