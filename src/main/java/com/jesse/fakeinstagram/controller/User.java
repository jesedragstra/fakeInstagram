package com.jesse.fakeinstagram.controller;

import com.jesse.fakeinstagram.exception.LoginException;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.service.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.http.HttpHeaders;

@RestController
public class User {
    UserService userService;
    @Autowired
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    @DeleteMapping("/deleteUser")
    public String deleteUser(HttpServletRequest request,
                                 HttpServletResponse response){
        String authorization = request.getHeader("Authorization");
        String token = authorization.substring(7);
        try{
             userService.deleteUser(token);
             return "{action:user deleted}";
        }catch(Exception e){
            throw new LoginException("Unable to delete User");
        }
    }
    @PostMapping("/signup")
    public InstagramUsers registerNewUser(@Valid @RequestBody InstagramUsers userRequest) throws Exception{
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        userRequest.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        InstagramUsers createdUser = userService.createUser(userRequest);
        return createdUser;

    }
}
