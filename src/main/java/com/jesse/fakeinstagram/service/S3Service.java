package com.jesse.fakeinstagram.service;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.springframework.web.multipart.MultipartFile;

public interface S3Service {
    public String save(final MultipartFile multipartFile,Long userId);
    public S3ObjectInputStream findByName(String fileName);
}
