package com.jesse.fakeinstagram.service;

import com.jesse.fakeinstagram.model.Post;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.UUID;

public interface PostService {
    public Post postPicture(String link, String username);
    public Post deletePost(UUID postId,String username);
    public List<Post> retrieveImages(String username);
}
