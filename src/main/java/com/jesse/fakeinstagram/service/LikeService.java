package com.jesse.fakeinstagram.service;

import com.jesse.fakeinstagram.model.LikesResponse;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

public interface LikeService {
    public void LikePost(String username, UUID postId);
    public void deleteLike(String username,UUID postId);
    public LikesResponse retrieveAllLikes(UUID postId);
}
