package com.jesse.fakeinstagram.service.internal;

import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    org.springframework.security.core.userdetails.User.UserBuilder builder = null;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        InstagramUsers findUser = userRepository.findByUsername(username);
        if(findUser == null){
            throw new UsernameNotFoundException(String.format(String.format("User with the username %s doesn't exist", username)));
        }
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.disabled(false);
        builder.password(findUser.getPassword());
        builder.authorities(new SimpleGrantedAuthority("ROLE_USER"));
        return builder.build();
    }
}
