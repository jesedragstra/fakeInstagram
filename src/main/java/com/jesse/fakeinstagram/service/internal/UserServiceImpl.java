package com.jesse.fakeinstagram.service.internal;

import com.jesse.fakeinstagram.exception.DeleteException;
import com.jesse.fakeinstagram.exception.SignupException;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.repository.UserRepository;
import com.jesse.fakeinstagram.service.UserService;
import com.jesse.fakeinstagram.util.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    JWTUtility jwtUtility;
    @Autowired
    public void setJwtUtility(JWTUtility jwtUtility){
        this.jwtUtility = jwtUtility;
    }
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public InstagramUsers createUser (InstagramUsers user)throws SignupException {
        InstagramUsers matchingUser = userRepository.findByEmail(user.getEmail());
        if(matchingUser != null){
            throw new SignupException("User with email: "+user.getEmail()+" already exists");
        }
        InstagramUsers savedUser = userRepository.save(user);
        return savedUser;
    }
    @Override
    public void deleteUser(String jwtToken) {
        try{
            String username = jwtUtility.getUsernameFromToken(jwtToken);
            userRepository.deleteByUsername(username);
        }catch(Exception e){
            System.out.println(e);
            throw new Error("Unable to delete user");
        }
    }

    @Override
    public org.springframework.security.core.userdetails.User loadByUsername(String username) throws UsernameNotFoundException {
            InstagramUsers currentUser = userRepository.findByUsername(username);
            if(currentUser==null){
                throw new DeleteException("Unable to delete user,user not found");
            }
            return new org.springframework.security.core.userdetails.User(currentUser.getUsername(),currentUser.getPassword(),new ArrayList<>());
    }
}
