package com.jesse.fakeinstagram.service.internal;

import com.jesse.fakeinstagram.exception.LikesException;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.model.LikesResponse;
import com.jesse.fakeinstagram.model.Post;
import com.jesse.fakeinstagram.repository.PostRepository;
import com.jesse.fakeinstagram.repository.UserRepository;
import com.jesse.fakeinstagram.service.LikeService;
import com.jesse.fakeinstagram.util.LikeJsonConverter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LikeServiceImpl implements LikeService {
    PostRepository postRepository;
    @Autowired
    public void setPostRepository(PostRepository postRepository){
        this.postRepository = postRepository;
    }
    UserRepository userRepository;
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public void LikePost(String username, UUID postId) {
        Optional<Post> findPost = postRepository.findById(postId);
        if(findPost.isEmpty()){
            throw new Error();
        }
       JSONObject likeObject = new JSONObject();
        List<String> likes = findPost.get().getLikes();
        try{
            if(!likes.isEmpty()){
                for(int i=0; i< likes.toArray().length;i++) {

                    JSONObject insideLike = new JSONObject(likes.get(i).toString());
                    String name = insideLike.getString("username");
                    if (name.equals(username)) {
                        throw new Error("Unable to like");

                    }
                }
            }
                likeObject.put("username",username);
                String likeObjectString = likeObject.toString();
                likes.add(likeObjectString);
                findPost.get().setLikes(likes);
                postRepository.save(findPost.get());

        }catch(Exception e){
            throw new LikesException("Unable to add like");
        }

    }

    @Override
    public void deleteLike(String username, UUID postId) {
        Optional<Post> findPost = postRepository.findById(postId);
        if(findPost.isEmpty()){
            throw new Error();
        }
        JSONObject likeObject = new JSONObject();
        List<String> likes = findPost.get().getLikes();
        try{
            for(int i=0; i< likes.toArray().length;i++) {

                JSONObject insideLike = new JSONObject(likes.get(i));
                if (insideLike.get("username").equals(username)) {
                    likes.remove(i);

                }
            }
            findPost.get().setLikes(likes);
            postRepository.save(findPost.get());

        }catch(Exception e){
            throw new LikesException("Unable to delete like");
        }

    }

    @Override
    public LikesResponse retrieveAllLikes(UUID postId) {
        int count = 0;
        Optional<Post> findPost = postRepository.findById(postId);
        if(findPost.isEmpty()){
            throw new Error();
        }
        List<String> likes = findPost.get().getLikes();
        for(int i=0; i< likes.toArray().length;i++) {
           count++;
        }
        LikesResponse likesResponse = new LikesResponse();
        likesResponse.setUsersLikes(likes);
        likesResponse.setLikeCount(count);
        return likesResponse;

    }

}
