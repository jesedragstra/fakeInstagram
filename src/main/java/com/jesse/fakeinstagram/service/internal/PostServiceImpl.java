package com.jesse.fakeinstagram.service.internal;

import com.jesse.fakeinstagram.exception.DeletePostException;
import com.jesse.fakeinstagram.exception.SavePostException;
import com.jesse.fakeinstagram.model.InstagramUsers;
import com.jesse.fakeinstagram.model.Post;
import com.jesse.fakeinstagram.repository.PostRepository;
import com.jesse.fakeinstagram.repository.UserRepository;
import com.jesse.fakeinstagram.service.PostService;
import com.jesse.fakeinstagram.service.S3Service;
import com.jesse.fakeinstagram.util.PostJsonConverter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.UUID;

@Service
public class PostServiceImpl implements PostService {
    UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    PostRepository postRepository;
    @Autowired
    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }
    PostJsonConverter postJsonConverter;
    @Autowired
    public void setPostJsonConverter(PostJsonConverter postJsonConverter) {
        this.postJsonConverter = postJsonConverter;
    }

    @Override
    public Post postPicture(String link,String username) {
        try {
            InstagramUsers instagramUsers = userRepository.findByUsername(username);
            Post insertPost = new Post();
            insertPost.setLink(link);
            insertPost.setUser(instagramUsers);
            return postRepository.save(insertPost);
        }catch (Exception e){
            throw new SavePostException("Unable to save picture");
        }

    }

    @Override
    public Post deletePost(UUID postId,String username) {
        try{
            InstagramUsers currentUser = userRepository.findByUsername(username);
            if(currentUser==null){
                throw new UsernameNotFoundException("Username not found");
            }
            Post post = postRepository.findByUserAndId(currentUser,postId);
            post.setUser(new InstagramUsers());
            postRepository.deleteByIdAndUser(postId,currentUser);
            if(post == null){
                throw new DeletePostException();
            }
            return post;

        }catch(Exception e){
            throw new DeletePostException();
        }
    }

    @Override
    public List<Post> retrieveImages(String username) {
         try{
             InstagramUsers user = userRepository.findByUsername(username);
             List<Post> posts = postRepository.findByUser(user);
             if(posts == null){
                 throw new SavePostException("Unable to retrieve posts");
             }
             return  posts;

         }catch(Exception e){
             throw new SavePostException("Unable to retrieve posts");
         }

    }
}
