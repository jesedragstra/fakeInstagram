package com.jesse.fakeinstagram.service;

import com.jesse.fakeinstagram.model.InstagramUsers;

import java.util.UUID;

public interface UserService {
    InstagramUsers createUser(InstagramUsers user) throws Exception;
    void deleteUser(String jwtToken);
    org.springframework.security.core.userdetails.User loadByUsername(String username);
}
